namespace InClothes.Model.Common
{
    public enum ProductTagEnum
    {
        New,
        Hit,
        Discount,
        Wholesale
    }
}