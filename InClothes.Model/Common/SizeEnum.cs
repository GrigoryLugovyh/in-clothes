namespace InClothes.Model.Common
{
    public enum SizeEnum
    {
        XS,
        S,
        M,
        L,
        XL,
        XXL,
        XXXL,
        XXXXL,
        XXXXXL
    }
}