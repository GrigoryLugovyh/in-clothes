using System.Collections.Generic;
using InClothes.Model.Common;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace InClothes.ViewModels
{
    public class ProductSizeViewModel
    {
        public SizeEnum CurrentSize { get; set; }
        public List<SelectListItem> SizeList { get; set; }
        public ProductSizeOptionViewModel SizeOptions { get; set; }
    }
}