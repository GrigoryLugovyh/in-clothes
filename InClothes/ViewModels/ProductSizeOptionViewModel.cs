using InClothes.Model.Common;

namespace InClothes.ViewModels
{
    public class ProductSizeOptionViewModel
    {
        public SizeTypeEnum Category { get; set; }
        public string[] SizeOptionHeaders { get; set; }
        public ProductSizeOption[] SizeOptions { get; set; }
    }
    
    public class ProductSizeOption
    {
        public string Breast { get; set; }
        public string Waists { get; set; }
        public SizeEnum Size { get; set; }
        public string SizeRus { get; set; }
    }
}