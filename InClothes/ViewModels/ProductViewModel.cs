using InClothes.Model.Common;

namespace InClothes.ViewModels
{
    public class ProductViewModel
    {
        public string ProductId { get; set; }
        public ProductSizeViewModel Size { get; set; }
        public ProductPriceViewModel Price { get; set; }
        public string Name { get; set; }
        public string Brand { get; set; }
        public string Article { get; set; }
        public string Description { get; set; }
        public string Composition { get; set; }
        public int Count { get; set; }
        public string[] Images { get; set; }
        public ProductTagEnum[] Tags { get; set; }
    }
}