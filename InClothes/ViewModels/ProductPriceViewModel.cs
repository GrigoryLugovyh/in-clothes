namespace InClothes.ViewModels
{
    public class ProductPriceViewModel
    {
        public decimal Price { get; set; }
        public decimal Discount { get; set; }
    }
}