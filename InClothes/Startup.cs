﻿using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using InClothes.ContainerModules;
using InClothes.Misc.ContainerModules;
using InClothes.Model.ContainerModules;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace InClothes
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    "default",
                    "{controller=Home}/{action=Index}/{id?}");
                
                routes.MapRoute(
                    "product",
                    "product/{id}",
                    new {controller = "Product", action = "Index"});
            });
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            var builder = new ContainerBuilder();
            builder.Populate(services);
            ConfigureContainer(builder);
            return new AutofacServiceProvider(builder.Build());
        }

        private static void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new ContainerModuleImplicit());
            builder.RegisterModule(new ContainerModuleModel());
            builder.RegisterModule(new ContainerModuleApi());
        }
    }
}