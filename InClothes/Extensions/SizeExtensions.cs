using System;
using InClothes.Model.Common;

namespace InClothes.Extensions
{
    public static class SizeExtensions
    {
        public static string GetSizeTypeDescription(this SizeTypeEnum sizeType)
        {
            switch (sizeType)
            {
                case SizeTypeEnum.Male:
                    return "Мужская ожежда";
                case SizeTypeEnum.Female:
                    return "Женская одежда";
                case SizeTypeEnum.Child:
                    return "Детская одежда";
                default:
                    throw new ArgumentOutOfRangeException(nameof(sizeType), sizeType, null);
            }
        }
    }
}