using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using InClothes.Model.Common;
using InClothes.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace InClothes.Controllers
{
    public class ProductController : Controller
    {
        private readonly ProductViewModel _productViewModel = new ProductViewModel
        {
            ProductId = "1",
            Name = "Брюки спортивные",
            Brand = "Simeon",
            Images = new[]
            {
                "/jpg/IMG_20190526_133220.jpg",
                "/jpg/IMG_20190526_134142.jpg",
                "/jpg/IMG_20190528_142424.jpg",
                "/jpg/IMG_20190528_142713.jpg",
                "/jpg/IMG_20190528_142721.jpg"
            },
            Tags = new[]
            {
                ProductTagEnum.New,
                ProductTagEnum.Discount,
                ProductTagEnum.Hit,
                ProductTagEnum.Wholesale
            },
            Price = new ProductPriceViewModel
            {
                Price = 110m,
                Discount = 15
            },
            Size = new ProductSizeViewModel
            {
                SizeOptions = new ProductSizeOptionViewModel
                {
                    Category = SizeTypeEnum.Child,
                    SizeOptionHeaders = new[]
                    {
                        "Российский размер",
                        "Размер производителя",
                        "Обхват груди, см",
                        "Обхват талии, см"
                    },
                    SizeOptions = new[]
                    {
                        new ProductSizeOption
                        {
                            Breast = "86-91",
                            Waists = "71-73",
                            Size = SizeEnum.S,
                            SizeRus = "44/46"
                        },
                        new ProductSizeOption
                        {
                            Breast = "96",
                            Waists = "76",
                            Size = SizeEnum.M,
                            SizeRus = "48"
                        }
                    }
                },
                SizeList = new List<SelectListItem>
                {
                    new SelectListItem
                    {
                        Value = SizeEnum.L.ToString(),
                        Text = SizeAsText(SizeEnum.L)
                    },
                    new SelectListItem
                    {
                        Value = SizeEnum.M.ToString(),
                        Text = SizeAsText(SizeEnum.M)
                    }
                },
                CurrentSize = SizeEnum.L
            },
            Count = 1,
            Article = "OS004EWEVAU4",
            Composition = "Хлопок - 98%, Эластан - 2%",
            Description = "тут должно быть описание, надо подумать, а нужно ли оно вообще и если да, то надо написать"
        };

        private static string SizeAsText(SizeEnum sizeEnum)
        {
            return $"44/46 - {sizeEnum}";
        }

        [HttpGet]
        public IActionResult Index(string id)
        {
            return View(_productViewModel);
        }

        [HttpPost]
        public async Task<PartialViewResult> CalculatePrice(
            string id, [FromQuery] string size, [FromQuery] string count)
        {
            var sizeEnum = Enum.Parse<SizeEnum>(size);

            int.TryParse(count, out var countInt);

            if (countInt < 1 || countInt > 9999)
                countInt = 1;

            decimal sizePrice;

            switch (sizeEnum)
            {
                case SizeEnum.XS:
                    sizePrice = 640;
                    break;
                case SizeEnum.S:
                    sizePrice = 675;
                    break;
                case SizeEnum.M:
                    sizePrice = 710;
                    break;
                case SizeEnum.L:
                    sizePrice = 765;
                    break;
                case SizeEnum.XL:
                    sizePrice = 790;
                    break;
                case SizeEnum.XXL:
                    sizePrice = 800;
                    break;
                case SizeEnum.XXXL:
                    sizePrice = 815;
                    break;
                case SizeEnum.XXXXL:
                    sizePrice = 830;
                    break;
                case SizeEnum.XXXXXL:
                    sizePrice = 845;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return PartialView("Product/_ProductPricePartial", new ProductPriceViewModel
            {
                Price = sizePrice * countInt
            });
        }
    }
}