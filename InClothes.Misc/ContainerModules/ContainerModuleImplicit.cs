using System;
using Autofac;

namespace InClothes.Misc.ContainerModules
{
    public class ContainerModuleImplicit : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assembliesToRegistration = AppDomain.CurrentDomain.GetAssemblies();

            builder.RegisterAssemblyTypes(assembliesToRegistration)
                .Where(type => type.FullName != null && type.FullName.StartsWith("InClothes"))
                .AsImplementedInterfaces();
        }
    }
}